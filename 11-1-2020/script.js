let sortLowToHigh = document.getElementById("sortLowToHigh");
let sortHighToLow = document.getElementById("sortHighToLow");
let text = document.getElementById("text");
function imgIphoneX() {
    let img = document.createElement("img"); 
    img.setAttribute("src", "images/iphonex.jpg");
    img.setAttribute("alt", "Iphone-x");
    img.setAttribute("class","card-img-top mobileImages");
    document.getElementById('iphoneX').appendChild(img); 
    
} 
function imgOneplus() { 
    let img = document.createElement("img");
    img.setAttribute("src", "images/onepluOneConcept.png");
    img.setAttribute("alt", "OnePlus One Concept");
    img.setAttribute("class","card-img-top mobileImages");
    document.getElementById('onePlus').appendChild(img); 
    
}  
function imgSamsung() { 
    let img = document.createElement("img");
    img.setAttribute("src", "images/samsungNote10.jpg");
    img.setAttribute("alt", "Samsung Note 10");
    img.setAttribute("class","card-img-top mobileImages");
    document.getElementById('samsung').appendChild(img); 
    
}  
function imgPoco() { 
    let img = document.createElement("img");
    img.setAttribute("src", "images/miPoco.jpg");
    img.setAttribute("alt", "Mi Poco F2");
    img.setAttribute("class","card-img-top mobileImages");
    document.getElementById('poco').appendChild(img); 
    
} 
function imgNokia() { 
    let img = document.createElement("img");
    img.setAttribute("src", "images/nokiaLumia.jpg");
    img.setAttribute("alt", "Nokia Lumia 730");
    img.setAttribute("class","card-img-top mobileImages");
    document.getElementById('nokiaLumia').appendChild(img); 

}   
function imgHtc() { 
    let img = document.createElement("img");
    img.setAttribute("src", "images/htcDesire.jpg");
    img.setAttribute("alt", "Htc Desire 820");
    img.setAttribute("class","card-img-top mobileImages");
    document.getElementById('htc').appendChild(img); 
    
}  
var mobilePhones = [
    { Brand :'Apple', Model : 'Iphone x', Cost : 70000, img : imgIphoneX() },
    { Brand :'OnePlus', Model : 'OnePust One Concept', Cost : 80000, img : imgOneplus()},
    { Brand :'Samsung', Model : 'Samsung Note 10', Cost : 60000, img : imgSamsung()},
    { Brand :'Mi', Model : 'Poco F2', Cost : 32000, img : imgPoco()},
    { Brand :'Nokia', Model : 'Nokia Lumia 730', Cost : 15000, img : imgNokia()},
    { Brand :'Htc', Model : 'Htc Desire 820', Cost : 17000, img : imgHtc()}
]
function sortLowToHighFun(){
    mobilePhones.sort(function(a,b){return a.Cost-b.Cost;});
    displayMobilePhones();
}
function sortHighToLowFun(){
    mobilePhones.sort(function(a,b){return b.Cost-a.Cost;});
    displayMobilePhones();
}
function sortAToZFun(){
    mobilePhones.sort(function(a,b){
        let x = a.Brand.toLowerCase();
        let y = b.Brand.toLowerCase();
        if (x < y){return -1};
        if (x > y){return 1};
        return 0;
    })
    displayMobilePhones();    
}
function sortZToAFun(){
    mobilePhones.sort(function(a,b){
        var x = a.Brand.toLowerCase();
        var y = b.Brand.toLowerCase();
        if(x < y){return 1;};
        if(x > y){return -1;};
        return 0 ;
    })
    displayMobilePhones();
}
function displayMobilePhones(){
    //for(let i=0;i<mobilePhones.length;i++){
        text.innerHTML = 
        mobilePhones[0].Brand+"- "+mobilePhones[0].Model+": "+mobilePhones[0].Cost+"<br>"+
        mobilePhones[1].Brand+"- "+mobilePhones[1].Model+": "+mobilePhones[1].Cost+"<br>"+
        mobilePhones[2].Brand+"- "+mobilePhones[2].Model+": "+mobilePhones[2].Cost+"<br>"+
        mobilePhones[3].Brand+"- "+mobilePhones[3].Model+": "+mobilePhones[3].Cost+"<br>"+
        mobilePhones[4].Brand+"- "+mobilePhones[4].Model+": "+mobilePhones[4].Cost+"<br>"+
        mobilePhones[5].Brand+"- "+mobilePhones[5].Model+": "+mobilePhones[5].Cost+"<br>";
    //}
}
